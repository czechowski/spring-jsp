<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page pageEncoding="UTF-8" %>

<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/js/jquery-3.1.1.js" var="jQuery" />
<spring:url value="/resources/js/bootstrap.js" var="bootstrapJs" />


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--Tutaj arkusz styli zlinkowany za pomoca c:url-->
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>">
<!--A tutaj arkusz styli zlinkowany za pomoca spring:url-->      
<link rel="stylesheet" href="${mainCss}">


<script src="${jQuery}"></script>
<script src="${bootstrapJs}"></script>