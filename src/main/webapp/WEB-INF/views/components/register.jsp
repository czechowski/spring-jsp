<%-- 
    Document   : register
    Created on : 2017-03-08, 17:27:47
    Author     : Tomasz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page pageEncoding="UTF-8" %>

<spring:url value="/" var="homeUrl"/>
<spring:url value="/travels" var="travelsUrl">
    <spring:param name="limit" value="10"></spring:param>
</spring:url>
<script>
    function Register()
    {
        var login = $('#login').val();
        var password = $('#password').val();
        $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "users",
        data: JSON.stringify({login: login, password: password}),
        dataType: 'json'
    }).done(function(data){
            $(windows).unload();
    }).fail(function(data){
        $("#valid").append(data);
    });
    }
   
</script>
<!DOCTYPE html>
 <div id="register" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-body">
                        <h3>Zarejestruj się</h3>
                        <p id="valid"></p>
                        <label>Nazwa użytkownika</label>
                        <p><input id="login" class="form-control"/></p>
                         <label>Hasło</label>
                         <p><input id="password" type="password"  class="form-control"/></p>
                         <h3 id ="a"></h3>
                    </div>
                    <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
                      <button class="btn btn-primary"  onclick="Register()">Rejestruj</button>
                    </div>
                     
                 </div>
            </div>
      </div>
