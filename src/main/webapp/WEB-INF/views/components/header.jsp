<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page pageEncoding="UTF-8" %>

<spring:url value="/" var="homeUrl"/>
<spring:url value="/travels" var="travelsUrl">
    <spring:param name="limit" value="10"></spring:param>
</spring:url>
<jsp:include page="../components/register.jsp" />

<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${homeUrl}">Traveler</a>
        </div>
        <div class="navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="${homeUrl}">Home</a></li>
                <li><a href="${travelsUrl}">Travels</a></li>
            </ul>
            <div class="navbar-form navbar-right hidden-xs">
            <a class="navbar-brand" href="#register" data-toggle="modal">Rejestracja</a>
            </div>
        </div>
      
    </div>
</nav>