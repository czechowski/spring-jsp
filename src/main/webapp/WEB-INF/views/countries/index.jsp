<%-- 
    Document   : index
    Created on : 2017-02-28, 20:30:16
    Author     : Tomasz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Countries</title>
        <jsp:include page="../components/meta.jsp" />
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />

        <div class="container">
            <h3>Jesteś na stronie countries/index.jsp</h3>
            <hr>

            <p>
                <c:url value="/countries/create" var="createUrl"/>
                <a class="btn btn-default" href="${createUrl}" role="button">Nowe państwo</a>
            </p>

            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th></th>
                </tr>
                <c:forEach items="${countries}" var="country">
                    <tr>
                        <td>${country.id}</td>
                        <td>${country.name}</td>
                        <td>
                            <c:url value="/countries/${country.id}" var="detailsUrl"/>
                            <a href="${detailsUrl}">Szczegóły</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>