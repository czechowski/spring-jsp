<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Travels - Details</title>
        <jsp:include page="../components/meta.jsp" />
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />
        
        
        
        <div class="container">
            <h3>Jesteś na stronie travels/details.jsp</h3>
            <hr>
            
            <dl class="dl-horizontal">
                <dt>ID</dt>
                <dd>${travel.id}</dd>
                <dt>Nazwa</dt>
                <dd>${travel.name}</dd>
                <dt>Opis</dt>
                <dd>${travel.description}</dd>
                <dt>Lista miejsc</dt>
                 <dd><c:forEach items="${travel.places}" var="place">
                    <tr>
                        <td>${place.name}</td>
                        <td>${place.city.name} - ${place.city.country.name}</td>
                    </tr>
                </c:forEach>
            </dd>
                <p>
                    <c:url value="/places/create/${travel.id}" var="createUrl"/>
                    <a class="btn btn-default" href="${createUrl}" role="button">Dodaj miejsce</a>
                </p>
            </dl>
        </div>
    </body>
</html>
