<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Travels - Create</title>
        <jsp:include page="../components/meta.jsp" />
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />

        <div class="container">
            <h3>Jesteś na stronie travels/create.jsp</h3>
            <hr>

            
            <form:form method="POST">
                <div class="form-group">
                    <form:label path="name">Nazwa</form:label>
                    <form:input class="form-control" path="name" />
                </div>
                
                <div class="form-group">
                    <form:label path="description">Opis</form:label>
                    <form:textarea path="description" class="form-control" rows="3" />
                </div>

                <button type="submit" class="btn btn-default">Zapisz</button>
            </form:form>
        </div>

    </body>
</html>
