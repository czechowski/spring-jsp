<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Travels</title>
        <jsp:include page="../components/meta.jsp" />
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />

        <div class="container">
            <h3>Jesteś na stronie travels/index.jsp</h3>
            <hr>

            <p>
                <c:url value="/travels/create" var="createUrl"/>
                <a class="btn btn-default" href="${createUrl}" role="button">Nowa podróż</a>
            </p>

            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th></th>
                </tr>
                <c:forEach items="${travels}" var="travel">
                    <tr>
                        <td>${travel.id}</td>
                        <td>${travel.name}</td>
                        <td>
                            <c:url value="/travels/${travel.id}" var="detailsUrl"/>
                            <a href="${detailsUrl}">Szczegóły</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
