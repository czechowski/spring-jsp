<%-- 
    Document   : index
    Created on : 2017-02-28, 20:29:46
    Author     : Tomasz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Cities</title>
        <jsp:include page="../components/meta.jsp" />
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />

        <div class="container">
            <h3>Jesteś na stronie cities/index.jsp</h3>
            <hr>

            <p>
                <c:url value="/cities/create" var="createUrl"/>
                <a class="btn btn-default" href="${createUrl}" role="button">Nowe miasto</a>
            </p>

            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th></th>
                </tr>
                <c:forEach items="${cities}" var="city">
                    <tr>
                        <td>${city.id}</td>
                        <td>${city.name}</td>
                        <td>${city.country.name}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
