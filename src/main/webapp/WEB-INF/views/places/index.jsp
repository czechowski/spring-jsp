<%-- 
    Document   : index
    Created on : 2017-02-28, 20:30:40
    Author     : Tomasz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Places</title>
        <jsp:include page="../components/meta.jsp" />
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />

        <div class="container">
            <h3>Jesteś na stronie places/index.jsp</h3>
            <hr>

            <p>
                <c:url value="/travels/create" var="createUrl"/>
                <a class="btn btn-default" href="${createUrl}" role="button">Nowe miejsce</a>
            </p>

            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Nazwa</th>
                    <th></th>
                </tr>
                <c:forEach items="${places}" var="place">
                    <tr>
                        <td>${place.id}</td>
                        <td>${place.name}</td>
                        <td>
                            <c:url value="/places/${place.id}" var="detailsUrl"/>
                            <a href="${detailsUrl}">Szczegóły</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>