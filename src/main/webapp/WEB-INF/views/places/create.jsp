<%-- 
    Document   : create
    Created on : 2017-02-28, 20:30:54
    Author     : Tomasz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Places - Create</title>
        <jsp:include page="../components/meta.jsp" />
        <script>
            $(window).unload(initCountryList()); 
            function initCountryList(){    
                
                $('#selectCountry').empty();
                $('#selectCountry').append('<option value="-1">Państwo</option>'); 
                 
                $.ajax("../../countries/list").done(function(data){
               var countries = data;
               for(var i =0; i<countries.length;i++)
                {      
                    $('#selectCountry').append('<option value="' + countries[i].id + '">'+ countries[i].name+'</option>');    
                }
                CountrySelect(countries[0].id);
                });
                
            }
            function CountrySelect(countryID)
            {

                $('#selectCity').empty();
                $.ajax('../../cities/ByCountry/' + countryID).done(function (data) {
                var cities = data;
                for(var i =0; i<cities.length;i++)
                {      
                    $('#selectCity').append('<option value="' + cities[i].id + '">'+ cities[i].name+'</option>');    
                }
                });  
            }
            var path = "";
            var ModalValue;
            function ModalCall(value)
            {
                ModalValue = value;
                $('#modalHeader').empty();
                if(value === '0')
                {
                    $('#modalHeader').append("Dodaj nowe państwo");
                    path = "../../countries/create/";
                }else
                {
                    $('#modalHeader').append("Dodaj nowe miasto");
                    path = "../../cities/create/";
                }
            }
            function Add()
            {
                var value = "";
                if(ModalValue === '0')
                {
                    value = $('#modalInput').val();
                }else
                {
                    value = $('#modalInput').val();
                    value += '&' + $('#selectCountry').val();
                }
                $.ajax(path + value);
                location.reload();
            }
        </script>
    </head>
    <body>
        <jsp:include page="../components/header.jsp" />

        <div class="container body-content">
            <h3>Jesteś na stronie places/create.jsp</h3>
            <hr>
            
            <div id="a"></div>
            <form:form method="POST" >
                <div class="form-horizontal">
                    <div class="col-md-10">
                        <p>
                          <form:label path="name" class="control-label col-md-2">Nazwa miejsca</form:label>
                          <form:input class="form-control" path="name" />
                        </p>
                     </div>
                       
                    
                    
                         <div class="col-md-10">   
                            <p>
                                <form:label path="name" class="control-label col-md-2">Wybierz państwo</form:label>
                                <form:select id="selectCountry" class="form-control" onchange="CountrySelect(this.value)" path="" >
                                </form:select>
                                <a href="#myModal" data-toggle="modal" onclick="ModalCall('0')">Dodaj brakujące państwo</a>
                                
                            </p>                    
                            
                        </div>
                   
                    
                      
                        <div class="col-md-10"> 
                            <p>      
                                <form:label path="name" class="control-label col-md-2">Wybierz miasto</form:label>
                                <form:select id="selectCity" class="form-control" path="city">
                            </form:select> 
                                <a id="cityHref" href="#myModal" data-toggle="modal" onclick="ModalCall('1')">Dodaj brakujące miasto</a>
                           </p>
                           
                        </div>
                   
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="submit" class="btn btn-default">Zapisz</button>
                        </div>
                    </div>
                </div>
                
            </form:form>
        </div>

        
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                 <div class="modal-content">
                    <div class="modal-body">
                        <h3 id="modalHeader"></h3>
                      <p><input id="modalInput" class="form-control"/></p>
                    </div>
                    <div class="modal-footer">
                      <button class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</button>
                      <button class="btn btn-primary"  onclick="Add()">Dodaj</button>
                    </div>
                 </div>
            </div>
      </div>

    </body>
</html>

  
    
