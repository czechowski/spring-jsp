/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.pawelczechowski.springandjsp.exception.ErrorResponse;
import pl.pawelczechowski.springandjsp.exception.UserArleadyExistsException;
import pl.pawelczechowski.springandjsp.model.User;
import pl.pawelczechowski.springandjsp.service.UserService;

/**
 *
 * @author Tomasz
 */
@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    /*
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "users/register",
        data: JSON.stringify({login: 'pawel', password: 'aaaa'}),
        dataType: 'json'
    })

    Jesli zwracamy void to Spring z automatu ustawi status na 404, dlatego tutaj ustawiamy zeby status byl 200.
    Proponuje też hashowanie hasła w metodzie serwisu a nie w User.setPassword
    */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void register(@Valid @RequestBody User user, BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        userService.create(user);
    }

    // Zwracam status 409 Conflict
    @ExceptionHandler(UserArleadyExistsException.class)
    @ResponseStatus(value = org.springframework.http.HttpStatus.CONFLICT)
    public @ResponseBody ErrorResponse userExistsHandler() {
        return new ErrorResponse(ErrorResponse.Type.GENERAL_ERROR, "Podana nazwa użytkownika jest zajęta");
    }

    // Zwracam status 400 Bad request
    @ExceptionHandler(BindException.class)
    @ResponseStatus(value = org.springframework.http.HttpStatus.BAD_REQUEST)
    public @ResponseBody List<ErrorResponse> validationErrorHandler(BindException ex) {
        List<ErrorResponse> result = new ArrayList<>();
        
        for (FieldError fieldError : ex.getFieldErrors()) {
            result.add(new ErrorResponse(ErrorResponse.Type.FIELD_VALIDATION_ERROR, fieldError.getField(), fieldError.getDefaultMessage()));
        }
        
        return result;
    }
    
    // Zwracam status 500 Internal server error
    @ExceptionHandler
    @ResponseStatus(value = org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ErrorResponse otherErrorsHandler(Exception ex) {
        return new ErrorResponse(ErrorResponse.Type.GENERAL_ERROR, "Nieznany błąd");
    }
}
