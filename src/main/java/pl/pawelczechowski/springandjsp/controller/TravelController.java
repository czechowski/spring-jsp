package pl.pawelczechowski.springandjsp.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.pawelczechowski.springandjsp.model.Travel;
import pl.pawelczechowski.springandjsp.service.TravelService;

@Controller
@RequestMapping("/travels")
public class TravelController {

    // Dzięki adnotacji @Autowired, Spring wstrzyknął nam TravelService automatycznie
    @Autowired
    private TravelService travelService;

    /*
        Wyświetla liste podróży, pobierając List<Travel> poprzez serwis TravelService
        Żeby przekazać dane modelu do widoku, zwracam ModelAndView
        Metoda przyjmuje limit wyświetlania obiektów na liscie.
        Parametry żądania można wstrzyknąć używając adnotacji @RequestParam.
        Mapuje sie na URL /travels?limit=10
     */
    @RequestMapping
    public ModelAndView index(@RequestParam int limit) {
        ModelAndView model = new ModelAndView();
        List<Travel> travels = travelService.list(limit);

        model.addObject("travels", travels);
        model.setViewName("travels/index");

        return model;
    }

    /*
        Wyświetla strone tworzenia nowej podróży
        Mapuje sie na URL /travels/create
     */
    @RequestMapping("/create")
    public ModelAndView create() {
        return new ModelAndView("travels/create", "command", new Travel());
    }
    
    /*
        Obsługuje zapisanie obiektu
        Mapuje sie na POST /travels/create
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView submit(@ModelAttribute Travel travel) {
        // Tu masz dostep do modelu wypelnionego przez usera, mozna go zapisac do bazy
        travelService.create(travel);
        // Zwraca widok szczegółów zapisanego modelu
        return new ModelAndView("travels/details", "travel", travel);
    }

    /*
        Wyświetla szczegóły podróży, pobierając Travel poprzez serwis TravelService
        Parametry URL można wstrzyknąć używając adnotacji @PathVariable.
        Mapuje sie na URL /travels/{id}
     */
    @RequestMapping("/{id}")
    public ModelAndView details(@PathVariable int id) {
        ModelAndView model = new ModelAndView();
        Travel travel = travelService.read(id);

        model.addObject("travel", travel);
        model.setViewName("travels/details");

        return model;
    }
}
