/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.controller;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.pawelczechowski.springandjsp.model.City;
import pl.pawelczechowski.springandjsp.model.Country;
import pl.pawelczechowski.springandjsp.model.Place;
import pl.pawelczechowski.springandjsp.service.CityService;
import pl.pawelczechowski.springandjsp.service.CountryService;
import pl.pawelczechowski.springandjsp.service.PlaceService;

/**
 *
 * @author Tomasz
 */
@Controller
@RequestMapping("/cities")
public class CityController {
    @Autowired
    private CityService cityService;
    @Autowired
    private CountryService countryService;

    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView model = new ModelAndView();
        List<City> cities = cityService.list();

        model.addObject("cities", cities);
        model.setViewName("cities/index");

        return model;
    }
    
    @RequestMapping(value = "/create/{param}")
    public void submit(@PathVariable String param) {
        String[] data = param.split("&");
        City city = new City(0,data[0],countryService.read(Integer.valueOf(data[1])));
        cityService.create(city);
    }
    
    @RequestMapping(value = "/ByCountry/{id}")
    public @ResponseBody List<City> ByCountry(@PathVariable int id)
    {
        List<City> cities = cityService.getCityByCountry(id);
        if(cities.size() > 1)
        {
            cities.sort(new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                     return ((City)o1).getName()
                            .compareTo(((City)o2).getName());
                }
            });
        }
        return cities;
    }

}
