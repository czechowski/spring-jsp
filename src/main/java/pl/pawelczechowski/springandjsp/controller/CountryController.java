/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.controller;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pl.pawelczechowski.springandjsp.model.Country;
import pl.pawelczechowski.springandjsp.service.CountryService;

/**
 *
 * @author Tomasz
 */
@Controller
@RequestMapping("/countries")
public class CountryController {
    @Autowired
    private CountryService countryService;

    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView model = new ModelAndView();
        List<Country> countries = countryService.list();

        model.addObject("countries", countries);
        model.setViewName("countries/index");

        return model;
    }

    @RequestMapping("/list")
    public @ResponseBody  List<Country>  list()
    {
        List<Country> countries = countryService.list();     
        if(countries.size() > 1)
        {
            countries.sort(new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                     return ((Country)o1).getName()
                            .compareTo(((Country)o2).getName());
                }
            });
        }
        return countries;
    }
    
    @RequestMapping(value = "/create/{param}")
    public @ResponseBody void submit(@PathVariable String param) {
        Country country = new Country(0,param);
        countryService.create(country);
    }

    
    @RequestMapping("/{id}")
    public ModelAndView details(@PathVariable int id) {
        ModelAndView model = new ModelAndView();
        Country country = countryService.read(id);

        model.addObject("country", country);
        model.setViewName("countries/details");

        return model;
    }
}
