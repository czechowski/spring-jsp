/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.pawelczechowski.springandjsp.model.City;
import pl.pawelczechowski.springandjsp.model.Country;
import pl.pawelczechowski.springandjsp.model.Place;
import pl.pawelczechowski.springandjsp.model.Travel;
import pl.pawelczechowski.springandjsp.service.CityService;
import pl.pawelczechowski.springandjsp.service.CountryService;
import pl.pawelczechowski.springandjsp.service.PlaceService;
import pl.pawelczechowski.springandjsp.service.TravelService;

/**
 *
 * @author Tomasz
 */
@Controller
@RequestMapping("/places")
public class PlaceController {
     @Autowired
    private PlaceService placeService;
     @Autowired
     private TravelService travelService;
     @Autowired
     private CityService cityService;
     @Autowired
     private CountryService countryService;

    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView model = new ModelAndView();
        List<Place> places = placeService.list();

        model.addObject("places", places);
        model.setViewName("places/index");

        return model;
    }
    
    @RequestMapping("/create/{id}")
    public ModelAndView create() {
        
        return new ModelAndView("/places/create", "command", new Place());
    }
    
    @RequestMapping(value = "/create/{id}", method = RequestMethod.POST)
    public ModelAndView submit(@ModelAttribute Place place, @PathVariable int id ) {
        Travel travel = travelService.read(id);
        place.setTravel(travel);
        place.setId(0);
        placeService.create(place);

        return new ModelAndView("redirect:/travels/"+id);
    }

    
   
}
