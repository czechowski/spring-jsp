/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import pl.pawelczechowski.springandjsp.model.City;
import pl.pawelczechowski.springandjsp.service.CityService;

/**
 *
 * @author Tomasz
 */

public class IndexToCityConverter implements Converter<String, City> {
    @Autowired
    CityService cityService;
    @Override
    public City convert(String source) {
        return cityService.read(Integer.valueOf(source));
    }

}