/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import pl.pawelczechowski.springandjsp.model.Country;

/**
 *
 * @author Tomasz
 */
@Service
public class CountryService {
     @PersistenceContext
    private EntityManager em;
     
     public List<Country> list()
     {
         return em.createQuery("select c from Country c").getResultList();
     }
     
       public Country read(int id) {
        return em.find(Country.class, id);
    }
    
    @Transactional
    public Country create(Country country) {
        em.persist(country);
        return country;
    }
}
