/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import pl.pawelczechowski.springandjsp.model.Place;

/**
 *
 * @author Tomasz
 */
@Service
public class PlaceService {
    @PersistenceContext
    private EntityManager em;
    
    public List<Place> list() {
        return em.createQuery("select p from Place p").getResultList();
    }

    public Place read(int id) {
        return em.find(Place.class, id);
    }
    
    @Transactional
    public Place create(Place place) {
        em.persist(place);
        
        return place;
    }
}
