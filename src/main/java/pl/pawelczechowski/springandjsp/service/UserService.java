/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import pl.pawelczechowski.springandjsp.exception.UserArleadyExistsException;
import pl.pawelczechowski.springandjsp.model.User;

/**
 *
 * @author Tomasz
 */
@Service
public class UserService {

    @PersistenceContext
    private EntityManager em;
    
    public List<User> list(int limit) {
        return em.createQuery("select u from User u").getResultList();
    }

    public User getUser(String login, String password) {
        User user = em.createQuery("SELECT u FROM User u WHERE login = :login AND password = :password", User.class)
                .setParameter("login", login)
                .setParameter("password", password)
                .getSingleResult();
        return user;
    }

    public boolean isUser(User user) {

        List<User> u = em.createQuery("SELECT u FROM User u WHERE login = :login", User.class)
                .setParameter("login", user.getLogin())
                .getResultList();
        if (!u.isEmpty()) {
            return true;
        }

        return false;
    }

    @Transactional
    public void create(User user) {
        if (isUser(user)) {
            throw new UserArleadyExistsException();
        }
        user.setPassword(createPasswordHash(user.getPassword()));
        em.persist(user);
    }

    private String createPasswordHash(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8"));
            byte[] digest = md.digest();
            return new String(digest);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
