/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pawelczechowski.springandjsp.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import pl.pawelczechowski.springandjsp.model.City;

/**
 *
 * @author Tomasz
 */
@Service
public class CityService {
    @PersistenceContext
    private EntityManager em;
     
    public List<City> list()
    {
        return em.createQuery("select c from City c").getResultList();
    }
     
    public City read(int id) {
        return em.find(City.class, id);
    }
    
    public List<City> getCityByCountry(int countryID)
    {
        List<City> cities = em.createQuery("select c from City c where c.country.id = :countryID").setParameter("countryID", countryID).getResultList();

        return cities;
    }
    
    @Transactional
    public City create(City city) {
        em.persist(city);
        return city;
    }
}
