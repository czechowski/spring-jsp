package pl.pawelczechowski.springandjsp.service;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import pl.pawelczechowski.springandjsp.model.Travel;

@Service
public class TravelService {
    
    @PersistenceContext
    private EntityManager em;
    
    public List<Travel> list(int limit) {
        return em.createQuery("select t from Travel t").getResultList();
    }

    public Travel read(int id) {
        return em.find(Travel.class, id);
    }
    
    @Transactional
    public Travel create(Travel travel) {
        em.persist(travel);
        return travel;
    }
}
