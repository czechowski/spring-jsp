package pl.pawelczechowski.springandjsp.exception;

public class ErrorResponse {
    public enum Type {
        GENERAL_ERROR, FIELD_VALIDATION_ERROR
    }
    
    private final Type type;
    private final String message;
    private String field;

    public ErrorResponse(Type type, String message) {
        this.type = type;
        this.message = message;
    }

    public ErrorResponse(Type type, String field, String message) {
        this.type = type;
        this.message = message;
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public Type getType() {
        return type;
    }

    public String getField() {
        return field;
    }
}
